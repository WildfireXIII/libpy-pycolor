#***************************************************************************
#
#  File: __init__.py (pycolor)
#  Date created: 05/22/2018
#  Date edited: 05/22/2018
#
#  Author: Nathan Martindale
#  Copyright © 2018 Digital Warrior Labs
#
#  Description: Contains all the color constants
#
#***************************************************************************

# ---- codes ----

# foreground
CODE_F_BLACK = 30
CODE_F_RED = 31
CODE_F_GREEN = 32
CODE_F_YELLOW = 33
CODE_F_BLUE = 34
CODE_F_MAGENTA = 35
CODE_F_CYAN = 36
CODE_F_WHITE = 37

# background
CODE_B_BLACK = 40
CODE_B_RED = 41
CODE_B_GREEN = 42
CODE_B_YELLOW = 43
CODE_B_BLUE = 44
CODE_B_MAGENTA = 45
CODE_B_CYAN = 46
CODE_B_WHITE = 47

# foreground (bright)
CODE_F_BLACK_BRIGHT = 90
CODE_F_RED_BRIGHT = 91
CODE_F_GREEN_BRIGHT = 92
CODE_F_YELLOW_BRIGHT = 93
CODE_F_BLUE_BRIGHT = 94
CODE_F_MAGENTA_BRIGHT = 95
CODE_F_CYAN_BRIGHT = 96
CODE_F_WHITE_BRIGHT = 97

# background (bright)
CODE_B_BLACK_BRIGHT = 100
CODE_B_RED_BRIGHT = 101
CODE_B_GREEN_BRIGHT = 102
CODE_B_YELLOW_BRIGHT = 103
CODE_B_BLUE_BRIGHT = 104
CODE_B_MAGENTA_BRIGHT = 105
CODE_B_CYAN_BRIGHT = 106
CODE_B_WHITE_BRIGHT = 107

# ---- string constants ----

RESET = '\033[0m'

# simple foreground colors
BLACK = '\033[' + str(CODE_F_BLACK) + 'm'
RED = '\033[' + str(CODE_F_RED) + 'm'
GREEN = '\033[' + str(CODE_F_GREEN) + 'm'
YELLOW = '\033[' + str(CODE_F_YELLOW) + 'm'
BLUE = '\033[' + str(CODE_F_BLUE) + 'm'
MAGENTA = '\033[' + str(CODE_F_MAGENTA) + 'm'
CYAN = '\033[' + str(CODE_F_CYAN) + 'm'
WHITE = '\033[' + str(CODE_F_WHITE) + 'm'

# simple bright foreground colors
BRIGHTBLACK = '\033[' + str(CODE_F_BLACK_BRIGHT) + 'm'
BRIGHTRED = '\033[' + str(CODE_F_RED_BRIGHT) + 'm'
BRIGHTGREEN = '\033[' + str(CODE_F_GREEN_BRIGHT) + 'm'
BRIGHTYELLOW = '\033[' + str(CODE_F_YELLOW_BRIGHT) + 'm'
BRIGHTBLUE = '\033[' + str(CODE_F_BLUE_BRIGHT) + 'm'
BRIGHTMAGENTA = '\033[' + str(CODE_F_MAGENTA_BRIGHT) + 'm'
BRIGHTCYAN = '\033[' + str(CODE_F_CYAN_BRIGHT) + 'm'
BRIGHTWHITE = '\033[' + str(CODE_F_WHITE_BRIGHT) + 'm'

# simple background colors
BLACK_BG = '\033[' + str(CODE_B_BLACK) + 'm'
RED_BG = '\033[' + str(CODE_B_RED) + 'm'
GREEN_BG = '\033[' + str(CODE_B_GREEN) + 'm'
YELLOW_BG = '\033[' + str(CODE_B_YELLOW) + 'm'
BLUE_BG = '\033[' + str(CODE_B_BLUE) + 'm'
MAGENTA_BG = '\033[' + str(CODE_B_MAGENTA) + 'm'
CYAN_BG = '\033[' + str(CODE_B_CYAN) + 'm'
WHITE_BG = '\033[' + str(CODE_B_WHITE) + 'm'

# simple bright background colors
BRIGHTBLACK_BG = '\033[' + str(CODE_B_BLACK_BRIGHT) + 'm'
BRIGHTRED_BG = '\033[' + str(CODE_B_RED_BRIGHT) + 'm'
BRIGHTGREEN_BG = '\033[' + str(CODE_B_GREEN_BRIGHT) + 'm'
BRIGHTYELLOW_BG = '\033[' + str(CODE_B_YELLOW_BRIGHT) + 'm'
BRIGHTBLUE_BG = '\033[' + str(CODE_B_BLUE_BRIGHT) + 'm'
BRIGHTMAGENTA_BG = '\033[' + str(CODE_B_MAGENTA_BRIGHT) + 'm'
BRIGHTCYAN_BG = '\033[' + str(CODE_B_CYAN_BRIGHT) + 'm'
BRIGHTWHITE_BG = '\033[' + str(CODE_B_WHITE_BRIGHT) + 'm'
